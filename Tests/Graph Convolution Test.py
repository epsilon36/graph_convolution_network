from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import tensorflow as tf
from tensorflow_graphics.notebooks import mesh_segmentation_dataio as dataio

from tensorflow_graphics.nn.layer import graph_convolution as graph_conv
import json
import numpy as np

TrainList = r"C:\Users\Arren Bustamante\Documents\OBJ Snapshots\Projection Profiling\System 2\TrainList.txt"
ModelPath = r"C:\Users\Arren Bustamante\Documents\OBJ Snapshots\Projection Profiling\System 2\Model Directory\Model2"
dataFolder = r'C:\Users\Arren Bustamante\Documents\Data'


Meshes = []
LandMarks = []
months = os.listdir(dataFolder)
#Iterate through all months in the folder.
Objects = []

def GenerateMesh(meshFile,landMarkFile):
    
    ######################Read Mesh File###########################################
    with open(meshFile,'r') as f:
        lines = f.readlines()
           
    _Vertices = []
#    Vertices = []
    Triangles = []
    Upper = []
    for line in lines:
        if line[0] == 'v':
            vertex = line.split()[1:4]
            _Vertices.append(vertex)
        if line[0] == 'f':
            triangle = line.split()[1:4]
            Triangles.append(triangle)
    Triangles = np.array(Triangles,dtype=np.int32)-1
    
    Vertices = _Vertices
    Vertices = np.array(Vertices,dtype=np.float32)
    Triangles = np.array(Triangles,dtype=np.int32)
    
    ##################Read Landmark file####################################################

    Upper = Vertices[0]
    
####################Obtain Distances#######################################    
#    Labels = np.zeros(len(Vertices),dtype=np.int64)
#    Labels[0] = 1
    
    
    Labels = np.zeros((len(Vertices),3),dtype=np.int32)
    for i in range(3): 
        distances = np.linalg.norm(Vertices-Upper[i],axis=1)
        Spheres = np.where(distances<.5,distances,0)
        sparse = np.nonzero(Spheres)
        for sp in sparse:
            try:
                Labels[sp,i] = 1
            except IndexError as e:
                print("here")
                print(e)
    
    Output = {'Vertices':Vertices,'Triangles':Triangles,'Labels':Labels}
    return Output,Vertices,Triangles,Labels

Vertices = []
Triangles = []
Labels = []


def ZeroPad(myList,mode='TV'):
    OutList = []
    Lengths = []
    maxLength = 0
    for i in range(len(myList)):
        length = myList[i].shape[0]
        Lengths.append(length)
        if length > maxLength:
            maxLength = length
    for i in range(len(myList)):
        z = maxLength-myList[i].shape[0]
        if mode == 'TV':
            outList = np.append(myList[i],np.zeros((z,3)),axis=0)
        elif mode == 'L':
            outList = np.append(myList[i],np.zeros((z)),axis=0)
        OutList.append(outList)
    return np.array(OutList),np.array(Lengths)



#################################
#upper = r"C:\Users\Arren Bustamante\Documents\OBJ Snapshots\Projection Profiling\TestTetraHedron.obj"
#upper = r"C:\Users\Arren Bustamante\Documents\OBJ Snapshots\Projection Profiling\IsoHedron.obj"
#upper = r"C:\Users\Arren Bustamante\Documents\OBJ Snapshots\Projection Profiling\SpikeTetraHedron.obj"
#landmark = r"C:\Users\Arren Bustamante\Documents\Data\2019.02\200242\landmark.lnd"
    
upper = r"C:\Users\Arren Bustamante\Documents\Data\2019.01\200130\upperGum.obj"
landmark = r"C:\Users\Arren Bustamante\Documents\Data\2019.01\200130\landmark.lnd"

Outputs,vertices,triangles,labels = GenerateMesh(upper,landmark)

N = 2
Triangles = np.array([triangles]*N)
_Vertices = np.array([vertices]*N)+np.random.normal(0,.1,(vertices.shape))
Labels = np.array([labels]*N)
print("Created labels")
#############################################
import quaternion
def Rotate(array,Q):
    if array.ndim == 2:
        _P = np.concatenate((np.zeros((len(array),1)),array),1)
    else:
        _P = np.concatenate((np.array([0]),array),axis=0)
    P = quaternion.as_quat_array(_P)
    Qi = Q.conj()
    result =  np.dot(np.dot(Q,P),Qi)
    if array.ndim == 2:
        return quaternion.as_float_array(result)[:,1:4]
    else:
        return quaternion.as_float_array(result)[1:4]
    
Vertices = []
for i in range(N):
    Q = quaternion.from_float_array(np.random.rand(4))
    mesh = Rotate(_Vertices[i],Q)
    Vertices.append(mesh)

Vertices = np.array(Vertices)

Vertices,VertLength = ZeroPad(Vertices)
Triangles,TriLength = ZeroPad(Triangles)
print("Get Triangles")


with tf.device('/gpu:0'):
 
    def SPDiag(dia):
        """Creates a sparse diagonal matrix given a diagonal, a tensor n dimension, and 
        the size of the square s """
        #This takes in the across the rows and return sthe diagonal sparse matix to account for points being adjacent to themselfs.
            #ns is the sparse portion of the dense batch dimension
            #s is the sparse portion or the number of vertices in the dense array
        dia = tf.cast(dia,tf.int64)
    
        diaShape = tf.shape(dia)
        n = diaShape[0]
        s = diaShape[1]
        
        ns = DepthVector(n,s,tf.int32)
    #    _x = np.tile(np.array(range(s)),(n))
        _x = tf.tile(tf.range(s),(n,))
        x = tf.expand_dims(_x,axis=1)
        y = x
    
        I = tf.cast(tf.concat((ns,x,y),axis=1),dtype=tf.int64)
        values = tf.reshape(dia,(-1,))
        Sparse = tf.SparseTensor(I,tf.cast(values,tf.float64),(n,s,s))
        
        _valuesInv2 = tf.div(.5,tf.cast(values,dtype=tf.float32))
        
        Infs = tf.math.is_inf(_valuesInv2)
        Bo = tf.math.logical_not(Infs)
        mask = tf.cast(Bo,tf.float32)
        
        valuesInv2 = tf.expand_dims(tf.multiply(_valuesInv2,mask),-1)
        
        ValuesInv2 = tf.reshape(valuesInv2,(n,s))
        
        return Sparse,tf.cast(ValuesInv2,tf.float64)
    
        
    def DepthVector(n,xy,a):
        """Calculates the Q array which creates the n dimension in a sparse array"""
        #We now need to take the first dimension of the original _indeces and use it as the zero axis dimension
            #The value of this dimension is the index of the mesh model.
        #Instantiate with Zeros.  The first t elements correspond to the first mesh
        #The next t elements correspond to the second mesh and so forth.
            #The index of i ends up being the coordinate and that coordinate is repeated t times.  
            #We concatenate this throughout the loop so that we end up with a vector that is n*t long.
            #Since the zero element is already instantiated, we go to the range of t-1
        XY = tf.cast(xy,a)
        _Ns = tf.range(n)
        Ns = tf.expand_dims(_Ns,-1)
        repeat = tf.tile(Ns,(1,XY))
        Q = tf.reshape(repeat,(-1,1))
        return tf.cast(Q,a)
    
    def MakeAdj(triangles,v):
        global batchSize
        BatchSize = tf.constant(batchSize,tf.int64)
        #Individual points of each triangle
        triangles = tf.cast(triangles,tf.int64)
        a = tf.expand_dims(triangles[:,:,0],2)
        b = tf.expand_dims(triangles[:,:,1],2)
        c = tf.expand_dims(triangles[:,:,2],2)
        
        #Define every edge (in both directions)
        A = tf.concat((a,b),axis=2)
        B = tf.concat((a,c),axis=2)
        C = tf.concat((b,c),axis=2)
        D = tf.concat((b,a),axis=2)
        E = tf.concat((c,a),axis=2)
        F = tf.concat((c,b),axis=2)
        
        #Combine all edges
        _indices = tf.concat((A,B,C,D,E,F),axis=1)
        #Flatten the array such that it takes the form [xy0,xy0,xy0,xy1,xy1,xy1]
        indices = tf.reshape(_indices,((-1,2)))
        
        origShape = tf.cast(tf.shape(_indices),tf.int64)
        #Generates a vector to create a sparse array to represent the n dimension of the dense matrix.
        Q = DepthVector(BatchSize,origShape[1],tf.int64)
    
        #Concatenate this vector to the indeces array
        Indices = tf.concat((Q,indices),axis=1)
        indexSize = tf.multiply(BatchSize,origShape[1])
        Values = tf.ones(indexSize,dtype=tf.float64)
        
        triShape = tf.cast(tf.add(tf.shape(triangles)[1],1),tf.int64)
        sparseShape = tf.stack((batchSize,V,V),axis=0)
        
        
        Sparse = tf.SparseTensor(Indices,Values,sparseShape)
        
        d = tf.sparse.reduce_sum(Sparse,axis=2)
        D,dI2 = SPDiag(d)
        Sparse = tf.sparse.add(Sparse,D)
        Adj = Sparse.__mul__(tf.expand_dims(dI2,2))
        
        return Adj


############################Mesh Encoder###########################################
sz = np.array([47426]*Vertices.shape[0])
#######################################Graph Start########################################################
v = Vertices.shape[1]
t = Triangles.shape[1]

V = tf.constant(v,tf.int64)

batchSize=2

###############################################
###############################################
_V0 = tf.placeholder(dtype=tf.float64,name="V0",shape=[batchSize,v,3])
_T = tf.placeholder(dtype=tf.int64,name='Triangles',shape=[batchSize,t,3])

#sizes = tf.constant(sz,dtype=tf.int64)
_sizes = tf.placeholder(dtype=tf.int64,name='sz',shape=[batchSize])
_L = tf.placeholder(dtype=tf.float64,name="labels",shape=[batchSize,v,3])

dV0 = tf.data.Dataset.from_tensor_slices(_V0)
#dadj_sp = tf.data.Dataset.from_tensor_slices(_adj_sp)
dT = tf.data.Dataset.from_tensor_slices(_T)
dsizes = tf.data.Dataset.from_tensor_slices(_sizes)
dL = tf.data.Dataset.from_tensor_slices(_L)

trainSet = tf.data.Dataset.zip((dV0,dT,dsizes,dL)).repeat().shuffle(500).batch(batchSize)
trainSet = trainSet.prefetch(1)
iterator = tf.data.Iterator.from_structure(trainSet.output_types,trainSet.output_shapes)
nextElement = iterator.get_next()

V0,T,sizes,L= nextElement
with tf.device('/gpu:0'):
    adj_sp = MakeAdj(T,V) 

sizes = None
print("Initialized placeholders")
###############################################
###############################################

#############################Redone Library###############################################

config = tf.ConfigProto(allow_soft_placement=True)
config.gpu_options.allow_growth = True




GV = {'u':{},'v':{},'c':{},'w':{},'b':{}}
with tf.device('/gpu:0'):

    
    def GraphConvolve(Data,A,index,C,D,W):
        C = int(Data.shape[-1])
        global GV,sizes
        ####Variables
        GV['u'][index] = tf.Variable(tf.truncated_normal((C,W),dtype=tf.float64))
        GV['v'][index] = tf.Variable(tf.truncated_normal((C,W),dtype=tf.float64))
        GV['c'][index] = tf.Variable(tf.truncated_normal((W,),dtype=tf.float64))
        GV['w'][index] = tf.Variable(tf.truncated_normal((W,C,D),dtype=tf.float64))
        GV['b'][index] = tf.Variable(tf.truncated_normal((D,),dtype=tf.float64))
        
        ####Flatten Data
    ################################## 
        if sizes != None:
    #    if False:
            sizes_square = sizes
            mask = tf.sequence_mask(sizes_square, Data.shape[-2])
            mask_indices = tf.cast(tf.where(mask), tf.int64)
            F = tf.reduce_sum(sizes)
            MI = tf.reshape(mask_indices,(F,2))
            x_flat = tf.gather_nd(params=Data, indices=MI)
    ##################################################    
        else:
            x_flat = tf.reshape(Data,(-1,V0.shape[-1]))
    
        #outShape = np.array(Data.shape)
#        outShape = tf.constant([Data.shape[0],Data.shape[1],D],dtype=tf.int64)
        outShape = tf.constant([batchSize,Data.shape[1],D],dtype=tf.int64)

        sparseShape = tf.cast(tf.shape(adj_sp),tf.int64)
        ########Flatten Adjacency matrix
        sparseAdj = tf.sparse.reshape(adj_sp,[-1,sparseShape[-2],sparseShape[-1]])
        indices = sparseAdj.indices
        
        ###Flatten Adj
    ###########################
        if sizes != None:
    #    if False:
            sizeSquare = tf.stack((sizes,sizes),axis=-1)
            cumsum = tf.cumsum(sizeSquare, axis=0, exclusive=True)
            index_shift = tf.gather(cumsum, indices[:, 0])
            indices = indices[:, 1:] + index_shift
            flatAdj = tf.SparseTensor(indices, sparseAdj.values,tf.reduce_sum(input_tensor=sizeSquare, axis=0))    
    
    ############################    
        else:
            index_shift = tf.multiply(tf.expand_dims(indices[:,0],-1),sparseShape[1:])
            flatIndices = tf.add(indices[:,1:],index_shift)
            flatAdj = tf.SparseTensor(flatIndices,sparseAdj.values,sparseShape[0]*sparseShape[1:])
    ##############################
        #Multiply flattened data values by linear variables
        x_u = tf.matmul(x_flat,GV['u'][index])
        x_v = tf.matmul(x_flat,GV['v'][index])
        
        #Get sparse adjacency matrix indices for Mx and My
        flatAdj_0 = flatAdj.indices[:,0]
        flatAdj_1 = flatAdj.indices[:,1]
        
        #Gather the appropriate data values corresponding to the adjacency matrix
        x_u_rep = tf.gather(x_u,flatAdj_0)
        x_v_sep = tf.gather(x_v,flatAdj_1)
        
        #soft assign
        weights_q = tf.exp(tf.add(tf.add(x_u_rep,x_v_sep),tf.reshape(GV['c'][index],(1,-1))))
        weights_q_sum = tf.reduce_sum(weights_q,axis=-1,keepdims=True)
        crossE = tf.divide(weights_q,weights_q_sum)
        x_sep = tf.gather(x_flat,flatAdj_1)
    ######################
        QList = tf.unstack(crossE,axis=-1)
        WList = tf.unstack(GV['w'][index],axis=0)
        def Partition_sum(data,group_ids,row_weights):
            num_rows = tf.size(input=group_ids, out_type=tf.int64)
            sparse_indices = tf.stack((group_ids, tf.range(num_rows)), axis=1)
            out_shape = (tf.reduce_max(input_tensor=group_ids) + 1, num_rows)
            sparse = tf.SparseTensor(sparse_indices, row_weights, dense_shape=out_shape)
            return tf.sparse.sparse_dense_matmul(sparse, data)
        
        y_i = []
        for q,w in zip(QList,WList):
            qm = tf.expand_dims(q,-1)
            product = tf.multiply(qm,x_sep)
            p_sum = Partition_sum(product,flatAdj_0,flatAdj.values)
            
            resultProduct = tf.matmul(p_sum,w)
            y_i.append(resultProduct)
        
        _y_out = tf.add(tf.add_n(y_i),tf.reshape(GV['b'][index],(1,-1)))
        
#############################################
        if sizes != None:
            y_out = tf.scatter_nd(mask_indices,_y_out,outShape)
        else:
            y_out = tf.reshape(_y_out,outShape)
        return y_out  
    

####################################################
###############################################
    convW1 = tf.Variable(tf.truncated_normal([1,3,16],dtype=tf.float64),name="convW1")
    convB1 = tf.Variable(tf.truncated_normal([16],dtype=tf.float64),name="convB1")
    conv1 = tf.add(tf.nn.conv1d(V0,convW1,1,'SAME',name="conv1"),convB1)
    
    _GC1 = graph_conv.feature_steered_convolution_layer(data=conv1,neighbors=adj_sp,sizes=sizes,translation_invariant=False,num_weight_matrices=8,num_output_channels=16)
    GC1 = tf.nn.relu(_GC1,name="GC1")
    
    _GC2 = graph_conv.feature_steered_convolution_layer(data=GC1,neighbors=adj_sp,sizes=sizes,translation_invariant=False,num_weight_matrices=8,num_output_channels=32)
    GC2 = tf.nn.relu(_GC2,name="GC2")
    
    _GC3 = graph_conv.feature_steered_convolution_layer(data=GC2,neighbors=adj_sp,sizes=sizes,translation_invariant=False,num_weight_matrices=8,num_output_channels=64)
    GC3 = tf.nn.relu(_GC3,name="GC3")
    
#    _GC4 = graph_conv.feature_steered_convolution_layer(data=GC3,neighbors=adj_sp,sizes=sizes,translation_invariant=False,num_weight_matrices=8,num_output_channels=64)
#    GC4 = tf.nn.relu(_GC4,name="GC4")
    
    convW2 = tf.Variable(tf.truncated_normal([1,64,128],dtype=tf.float64),name="convW2")
    convB2 = tf.Variable(tf.truncated_normal([128],dtype=tf.float64),name="convB2")
    conv2 = tf.add(tf.nn.conv1d(GC3,convW2,1,'SAME',name="conv2"),convB2)
    
    convW3 = tf.Variable(tf.truncated_normal([1,128,3],dtype=tf.float64),name="convW3")
    convB3 = tf.Variable(tf.truncated_normal([3],dtype=tf.float64),name="convB3")
    conv3 = tf.add(tf.nn.conv1d(conv2,convW3,1,'SAME',name="conv3"),convB3)
    
    Guess = tf.nn.softmax(conv3,axis=1)
    
    Loss = tf.reduce_sum(tf.nn.softmax_cross_entropy_with_logits_v2(labels=L,logits=conv3,axis=1))
    
    optimizer = tf.train.AdamOptimizer(learning_rate=.001).minimize(Loss)
print("loaded graph")  
init_op = tf.global_variables_initializer()
training_init_op = iterator.make_initializer(trainSet,name="InitTrain")
###################################Session Definition#############################
#sess = tf.InteractiveSession(config=config)
#tf.global_variables_initializer().run()
#fd = {_V0:Vertices,_T:Triangles,_L:Labels,_sizes:sz}
#sess.run(training_init_op,feed_dict=fd)


##################################################
####################################################
sess = tf.Session(config=config)

sess.run(tf.global_variables_initializer())
print("First initialization")
#fd = {_V0:Vertices,_adj_sp:(Adj.coords.T,Adj.data,Adj.shape),_L:Labels,_sizes:sz}
fd = {_V0:Vertices,_T:Triangles,_L:Labels,_sizes:sz}

sess.run(training_init_op,feed_dict=fd)
print("Second initialization")
epochs = 400
for i in range(epochs):
    _,Q = sess.run([optimizer,Loss])
    print(i,Q)



