"""Experiment with reducing convs and adding GCs
Uses imported reduced mesh files
(See 6c for best running model)
This model is designed to record GC weights
Uses Classic Relu 
Uses antagonistic weighting.
Connects to executable subprocess to lower resolutions
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import tensorflow as tf
#from tensorflow_graphics.notebooks import mesh_segmentation_dataio as dataio

from tensorflow_graphics.nn.layer import graph_convolution as graph_conv
import json
import numpy as np


import subprocess


TrainList = r"C:\Users\Arren Bustamante\Documents\OBJ Snapshots\Projection Profiling\System 2\TrainList.txt"
ModelPath = r"C:\Users\Arren Bustamante\Documents\OBJ Snapshots\Projection Profiling\System 2\Model Directory\Model2"
dataFolder = r'C:\Users\Arren Bustamante\Documents\Data'

Meshes = []
LandMarks = []
months = os.listdir(dataFolder)
#Iterate through all months in the folder.
Objects = []

def FilterMesh(_vertices,_triangles):
    """This function cleans out the unused vertices from the mesh and reindexes the faces to account for the new vertex positions"""
    #Consolidate the 3 rows of triangle faces into one dimension
    _T_set = np.reshape(_triangles,(-1))
    #Convert 1d array -> set -> array to get rid of duplicates
    T_set = np.array(list(set(_T_set)))
    
    #Assume that the indices of T_set are in the order of the original points
    #and the values of T_set are the new positions of these points.
    
    #Define a dictionary to go from the new position back to the old position
    invT_set = {}
    for i in range(len(T_set)):
        invT_set[T_set[i]] = i
    
    #Build a list of each vertex in the T_set and only the vertices that are included in the set.
    vs = []
    for i in range(len(T_set)):
        vs.append(_vertices[T_set[i]])
        
    #Build a new list of triangles by putting in the old indices and obtaining the new indices for the points.
    ts = []
    for i in range(len(_triangles)):
        tjs = []
        for j in range(3):
            tj = _triangles[i][j]
            tjs.append(invT_set[tj])
        ts.append(tjs)
    
    return np.array(vs,dtype=np.float64),np.array(ts,dtype=np.int64)

def GenerateMesh(meshFile,landMarkFile):
    global myFile
    myFile = meshFile
    
    ######################Read Mesh File###########################################
    with open(meshFile,'r') as f:
        lines = f.readlines()
           
    _Vertices = []
#    Vertices = []
    Triangles = []
    Upper = []
    for line in lines:
        if line[0] == 'v':
            vertex = line.split()[1:4]
            _Vertices.append(vertex)
        if line[0] == 'f':
            triangle = line.split()[1:4]
            Triangles.append(triangle)
    triangles = np.array(Triangles,dtype=np.int64)-1
    
    vertices = _Vertices
            
    vertices = np.array(vertices,dtype=np.float64)
    triangles = np.array(triangles,dtype=np.int64)
    
    Vertices,Triangles = FilterMesh(vertices,triangles)
    
    ##################Read Landmark file####################################################
    #Open and read files
    f = open(landmark,'r')
    lines = f.readlines()
    f.close()
    #Landmarks are located in line 1
    line = lines[1].split()
    
    Upper = []
    #lowerPoints = []
    #The first three poitns listed are upper, the last two are lower.  
        #Parse the text and save values as numpy arrays.
    for i in range(0,3):
        Upper.append(line[3*i:3*i+3])
    #for i in range(3,5):
    #    lowerPoints.append(line[3*i+9:3*i+12])
    Upper = np.array(Upper,dtype=float)
    
    #lowerPoints = np.array(lowerPoints,dtype=float)
    
####################Obtain Distances#######################################    
    Labels = np.zeros((len(Vertices),3),dtype=np.int32)

    for i in range(3): 
        distances = np.linalg.norm(Vertices-Upper[i],axis=1)
        #Find only the closest point
#        minDistance = np.min(distances)
        minDistance = 2.5
        Spheres = np.where(distances<=minDistance,distances,0)
        sparse = np.nonzero(Spheres)
        for sp in sparse:
            try:
                Labels[sp,i] = 1
            except IndexError as e:
                print("here")
                print(e)
    _P = 1-np.sum(Labels,axis=1)
    P = np.expand_dims(_P,0).T
    Labels = np.concatenate((Labels,P),axis=1)
    Output = {'Vertices':Vertices,'Triangles':Triangles,'Labels':Labels}
    return Output,Vertices,Triangles,Labels

def ReduceFaces(Input,Faces=1600):
    reductionExe = r"C:\Users\Arren Bustamante\Documents\Simplify2.exe"
    OutputFolder = r"C:\Users\Arren Bustamante\Documents\Data\Low Res"
    
    standardName = os.path.normpath(Input)
    folderName,fileName = standardName.split(os.sep)[-2:]
    
    outFile = os.path.join(OutputFolder,folderName+" "+fileName.replace('.obj',' ')+"Res "+str(Faces)+".obj")
    
    commandString = '"'+reductionExe+'"'+' '+'"'+Input+'"'+' '+'"'+outFile+'"'+' 1.0 7 1600'
    subprocess.call(commandString)
    return outFile

Vertices = []
Triangles = []
Labels = []

with open(TrainList,'r') as f:
    JobsList = f.readlines()
for i in range(len(JobsList)):
    JobsList[i] = JobsList[i].replace('\n','')

for month in months:
#if True:
    #Get the folder pathway for the given month
    folder = os.path.join(dataFolder,month)
    #Get a list of the jobs within that folder
    jobs = os.listdir(folder)
    #Iterate through jobs
    for job in jobs:
        if job in JobsList:
            try:
    #            if str(job) == '200224':
                if len(Meshes) < 10:
                    jobFolder = os.path.join(folder,job)
                    #Get the text file with the job info.
                    textFile = os.path.join(jobFolder,str(job+".DPRJ"))
                    dt = json.load(open(textFile))['filePaths']
        
                    if os.path.isfile(textFile):
                        
                        #Get the appropriate files listed in the text file.                    
                        upper = os.path.join(jobFolder,dt['UPPER_GUM'])
                        lower = os.path.join(jobFolder,dt['LOWER_GUM'])
                        #bite = os.path.join(jobFolder,dt['BITE'])
                        landmark = os.path.join(jobFolder,dt['LANDMARK'])
                        if os.path.isfile(landmark) and os.path.isfile(upper):# and os.path.isfile(lower):
                            lowResUpper = ReduceFaces(upper)
                            Meshes.append(lowResUpper)
                            print(lowResUpper)
                            LandMarks.append(landmark)
                            Outputs,vertices,triangles,labels = GenerateMesh(lowResUpper,landmark)
                            if vertices.shape[0] < 30000:
                                Vertices.append(vertices)
                                Triangles.append(triangles)
                                Labels.append(labels)
                                Objects.append(Outputs)
                                print(job)
                                        
            except FileNotFoundError:
                pass




def ZeroPad(myList,mode='V'):
    OutList = []
    Lengths = []
    maxLength = 0
    for i in range(len(myList)):
        length = myList[i].shape[0]
        Lengths.append(length)
        if length > maxLength:
            maxLength = length
    print("Finished padding loop 1")
    for i in range(len(myList)):
        z = maxLength-myList[i].shape[0]
        if mode == 'V':
            outList = np.append(myList[i],np.zeros((z,3)),axis=0)
        elif mode == 'L':
            outList = np.append(myList[i],np.zeros((z,4)),axis=0)
        elif mode == 'T':
            outList = np.append(myList[i],np.ones((z,3)),axis=0)
        OutList.append(outList)
    print("Finished Padding loop 2")
    return np.array(OutList),np.array(Lengths)


#############################################
print("Zero Padding")
Vertices,VertLength = ZeroPad(Vertices)
Triangles,TriLength = ZeroPad(Triangles,mode='V')
Labels,sz = ZeroPad(Labels,mode='L')


############################################





#with tf.device('/gpu:0'):
if True: 
    def SPDiag(dia):
        with tf.device("/gpu:0"):
            """Creates a sparse diagonal matrix given a diagonal, a tensor n dimension, and 
            the size of the square s """
            #This takes in the across the rows and return sthe diagonal sparse matix to account for points being adjacent to themselfs.
                #ns is the sparse portion of the dense batch dimension
                #s is the sparse portion or the number of vertices in the dense array
            dia = tf.cast(dia,tf.int64)
        
            diaShape = tf.shape(dia)
            n = diaShape[0]
            s = diaShape[1]
            
            ns = DepthVector(n,s,tf.int32)
        #    _x = np.tile(np.array(range(s)),(n))
            _x = tf.tile(tf.range(s),(n,))
            x = tf.expand_dims(_x,axis=1)
            y = x
        
            I = tf.cast(tf.concat((ns,x,y),axis=1),dtype=tf.int64)
            values = tf.reshape(dia,(-1,))
            Sparse = tf.SparseTensor(I,tf.cast(values,tf.float64),(n,s,s))
            
            _valuesInv2 = tf.div(.5,tf.cast(values,dtype=tf.float32))
            
            Infs = tf.math.is_inf(_valuesInv2)
            Nans = tf.math.is_nan(_valuesInv2)
            Bads = tf.math.logical_or(Infs,Nans)
#            Bo = tf.math.logical_not(Bads)
#            mask = tf.cast(Bo,tf.float32)
#            valuesInv2 = tf.expand_dims(tf.multiply(_valuesInv2,mask),-1)
            valuesInv2 = tf.where(Bads,tf.zeros(_valuesInv2.shape[0],dtype=tf.float32),_valuesInv2)

            ValuesInv2 = tf.reshape(valuesInv2,(n,s))
            
            return Sparse,tf.cast(ValuesInv2,tf.float64)
    
        
    def DepthVector(n,xy,a):
        """Calculates the Q array which creates the n dimension in a sparse array"""
        #We now need to take the first dimension of the original _indeces and use it as the zero axis dimension
            #The value of this dimension is the index of the mesh model.
        #Instantiate with Zeros.  The first t elements correspond to the first mesh
        #The next t elements correspond to the second mesh and so forth.
            #The index of i ends up being the coordinate and that coordinate is repeated t times.  
            #We concatenate this throughout the loop so that we end up with a vector that is n*t long.
            #Since the zero element is already instantiated, we go to the range of t-1
        XY = tf.cast(xy,a)
        _Ns = tf.range(n)
        Ns = tf.expand_dims(_Ns,-1)
        repeat = tf.tile(Ns,(1,XY))
        Q = tf.reshape(repeat,(-1,1))
        return tf.cast(Q,a)
    def Repeat(vector,times):
        expanded = tf.expand_dims(vector,-1)
        tiled = tf.tile(expanded,(1,times))
        unfolded = tf.reshape(tiled,(-1,))
        return unfolded
    
    def MakeAdj(triangles,v,sizes):
        global batchSize

        BatchSize = tf.constant(batchSize,tf.int64)
        vv = tf.cast(tf.squeeze(tf.subtract(v,0)),dtype=tf.int64)
        s = tf.squeeze(sizes)
        #Individual points of each triangle
        triangles = tf.cast(triangles,tf.int64)
        a = tf.expand_dims(triangles[:,:,0],2)
        b = tf.expand_dims(triangles[:,:,1],2)
        c = tf.expand_dims(triangles[:,:,2],2)
        
        #Define every edge (in both directions)
        A = tf.concat((a,b),axis=2)
        B = tf.concat((a,c),axis=2)
        C = tf.concat((b,c),axis=2)
        D = tf.concat((b,a),axis=2)
        E = tf.concat((c,a),axis=2)
        F = tf.concat((c,b),axis=2)
        
        #Combine all edges
        _indices = tf.concat((A,B,C,D,E,F),axis=1)
        #Flatten the array such that it takes the form [xy0,xy0,xy0,xy1,xy1,xy1]
        indices = tf.reshape(_indices,((-1,2)))
        
        origShape = tf.cast(tf.shape(_indices),tf.int64)
        #Generates a vector to create a sparse array to represent the n dimension of the dense matrix.
        Q = DepthVector(BatchSize,origShape[1],tf.int64)
    
        #Concatenate this vector to the indeces array
        Indices = tf.concat((Q,indices),axis=1)
        indexSize = tf.multiply(BatchSize,origShape[1])
        Values = tf.ones(indexSize,dtype=tf.float64)
        
#        triShape = tf.cast(tf.add(tf.shape(triangles)[1],1),tf.int64)
        sparseShape = tf.stack((batchSize,V,V),axis=0)
        
        
        Sparse = tf.SparseTensor(Indices,Values,sparseShape)
        d = tf.sparse.reduce_sum(Sparse,axis=2)
        D,dI2 = SPDiag(d)
        Sparse = tf.sparse.add(Sparse,D)
        Adj = Sparse.__mul__(tf.expand_dims(dI2,2))
        
        def Bufferize(Adj,s,vv):
        
            buffer = tf.range(tf.squeeze(tf.add(s-1,1)),vv)
            
            Xs1 = Repeat(buffer,vv)
            Ys1 = tf.tile(tf.range(0,vv),(vv-s,))
            
            Xs2 = Repeat(tf.range(0,s),(vv-s))
            Ys2 = tf.tile(buffer,(s,))
            
            Xs = tf.concat((Xs1,Xs2),0)
            Ys = tf.concat((Ys1,Ys2),0)
            Ns = tf.zeros(tf.shape(Xs),dtype=tf.int64)
            
            dummyIndices = tf.transpose(tf.stack((Ns,Xs,Ys)))
            dummyValues = tf.ones(tf.shape(Xs),dtype=tf.float64)
            
            Dummy = tf.SparseTensor(dummyIndices,dummyValues,sparseShape)
            
            ADJ = tf.sparse.add(Dummy,Adj)
            return ADJ
        
        Result = tf.cond(tf.greater(vv,s),lambda: Bufferize(Adj,s,vv),lambda: Adj)
        return Result


############################Mesh Encoder###########################################
#######################################Graph Start########################################################
t = Triangles.shape[1]
v = Vertices.shape[1]
V = tf.constant(v,tf.int64)

batchSize=1

###############################################
###############################################
_V0 = tf.placeholder(dtype=tf.float64,name="V0",shape=[None,v,3])
_T = tf.placeholder(dtype=tf.int64,name='Triangles',shape=[None,t,3])

#sizes = tf.constant(sz,dtype=tf.int64)
_sizes = tf.placeholder(dtype=tf.int64,name='sz',shape=[None])
_L = tf.placeholder(dtype=tf.float64,name="labels",shape=[None,v,4])

dV0 = tf.data.Dataset.from_tensor_slices(_V0)
#dadj_sp = tf.data.Dataset.from_tensor_slices(_adj_sp)
dT = tf.data.Dataset.from_tensor_slices(_T)
dsizes = tf.data.Dataset.from_tensor_slices(_sizes)
dL = tf.data.Dataset.from_tensor_slices(_L)

trainSet = tf.data.Dataset.zip((dV0,dT,dsizes,dL)).repeat().shuffle(10).batch(batchSize)
trainSet = trainSet.prefetch(1)
iterator = tf.data.Iterator.from_structure(trainSet.output_types,trainSet.output_shapes)
nextElement = iterator.get_next()

V0,T,sizes,L= nextElement
with tf.device('/gpu:0'):
    adj_sp = MakeAdj(T,V,sizes) 

#sizes = None
print("Initialized placeholders")
##########################################################################################
##########################################################################################
#############################Redone Library###############################################

config = tf.ConfigProto(allow_soft_placement=True)
config.gpu_options.allow_growth = True

#######

def GraphConvolve(data,Adj,index,C,D,W,sizes=sizes):
    SS = sizes
    Data = data
#    C = int(Data.shape[-1])
    global GV,batchSize
    ####Variables
    GV['u'][index] = tf.Variable(tf.truncated_normal((C,W),dtype=tf.float64))
    GV['v'][index] = tf.Variable(tf.truncated_normal((C,W),dtype=tf.float64))
    GV['c'][index] = tf.Variable(tf.truncated_normal((W,),dtype=tf.float64))
    GV['w'][index] = tf.Variable(tf.truncated_normal((W,C,D),dtype=tf.float64))
    GV['b'][index] = tf.Variable(tf.truncated_normal((D,),dtype=tf.float64))

    ####Flatten Data
################################## 
    if SS != None:
#    if False:
        sizes_square = sizes
        mask = tf.sequence_mask(sizes_square, Data.shape[-2])
        mask_indices = tf.cast(tf.where(mask), tf.int64)
        F = tf.reduce_sum(sizes)
        MI = tf.reshape(mask_indices,(F,2))
        x_flat = tf.gather_nd(params=Data, indices=MI)
        
##################################################    
    else:
        x_flat = tf.reshape(Data,(-1,C))

    #outShape = np.array(Data.shape)
    outShape = tf.constant([batchSize,v,D],dtype=tf.int64)
    sparseShape = tf.cast(tf.shape(adj_sp),tf.int64)
    ########Flatten Adjacency matrix
    sparseAdj = tf.sparse.reshape(adj_sp,[-1,sparseShape[-2],sparseShape[-1]])
    indices = sparseAdj.indices
    
    
    ###Flatten Adj
###########################
    if SS != None:
#    if False:
        sizeSquare = tf.stack((sizes,sizes),axis=-1)
        cumsum = tf.cumsum(sizeSquare, axis=0, exclusive=True)
        index_shift = tf.gather(cumsum, indices[:, 0])
        indices = indices[:, 1:] + index_shift
        flatAdj = tf.SparseTensor(indices, sparseAdj.values,tf.reduce_sum(input_tensor=sizeSquare, axis=0))    

############################    
    else:
        index_shift = tf.multiply(tf.expand_dims(indices[:,0],-1),sparseShape[1:])
        flatIndices = tf.add(indices[:,1:],index_shift)
        flatAdj = tf.SparseTensor(flatIndices,sparseAdj.values,sparseShape[0]*sparseShape[1:])
##############################
    #Multiply flattened data values by linear variables
    x_u = tf.matmul(x_flat,GV['u'][index])
    x_v = tf.matmul(x_flat,GV['v'][index])
    
    #Get sparse adjacency matrix indices for Mx and My
    flatAdj_0 = flatAdj.indices[:,0]
    flatAdj_1 = flatAdj.indices[:,1]
    
    #Gather the appropriate data values corresponding to the adjacency matrix
    x_u_rep = tf.gather(x_u,flatAdj_0)
    x_v_sep = tf.gather(x_v,flatAdj_1)
    
    #soft assign
    weights_q = tf.exp(tf.add(tf.add(x_u_rep,x_v_sep),tf.reshape(GV['c'][index],(1,-1))))
    weights_q_sum = tf.reduce_sum(weights_q,axis=-1,keepdims=True)
    crossE = tf.divide(weights_q,weights_q_sum)
    x_sep = tf.gather(x_flat,flatAdj_1)
######################
    QList = tf.unstack(crossE,axis=-1)
    WList = tf.unstack(GV['w'][index],axis=0)
    def Partition_sum(data,group_ids,row_weights):
        num_rows = tf.size(input=group_ids, out_type=tf.int64)
        sparse_indices = tf.stack((group_ids, tf.range(num_rows)), axis=1)
        out_shape = (tf.reduce_max(input_tensor=group_ids) + 1, num_rows)
        sparse = tf.SparseTensor(sparse_indices, row_weights, dense_shape=out_shape)
        return tf.sparse.sparse_dense_matmul(sparse, data)
    
    y_i = []
    for q,w in zip(QList,WList):
        qm = tf.expand_dims(q,-1)
        product = tf.multiply(qm,x_sep)
        p_sum = Partition_sum(product,flatAdj_0,flatAdj.values)
        
        resultProduct = tf.matmul(p_sum,w)
        y_i.append(resultProduct)
    
    _y_out = tf.add(tf.add_n(y_i),tf.reshape(GV['b'][index],(1,-1)))
    
#############################################
    if SS != None:
        y_out = tf.scatter_nd(MI,_y_out,outShape)
    else:
        y_out = tf.reshape(_y_out,outShape)
    return y_out  
    
GV = {'u':{},'v':{},'c':{},'w':{},'b':{}}
with tf.device('/gpu:0'):
    
  
##########################################################################################
##########################################################################################
    global_step = tf.Variable(0,trainable=False,dtype=tf.int64)

#    _GC1 = graph_conv.feature_steered_convolution_layer(data=V0,neighbors=adj_sp,sizes=sizes,translation_invariant=True,num_weight_matrices=32,num_output_channels=16)
    _GC1 = GraphConvolve(V0,adj_sp,0,8,16,32,sizes=sizes)
    GC1 = tf.nn.relu(_GC1,name="GC1")
    
    _GC2 = graph_conv.feature_steered_convolution_layer(data=GC1,neighbors=adj_sp,sizes=sizes,translation_invariant=True,num_weight_matrices=32,num_output_channels=32)
#    _GC2 = GraphConvolve(GC1,adj_sp,1,16,32,8)
    GC2 = tf.nn.relu(_GC2,name="GC2")
    
    _GC3 = graph_conv.feature_steered_convolution_layer(data=GC2,neighbors=adj_sp,sizes=sizes,translation_invariant=True,num_weight_matrices=32,num_output_channels=64)
#    _GC3 = GraphConvolve(GC2,adj_sp,2,32,64,8)
    GC3 = tf.nn.relu(_GC3,name="GC3")
                      
    _GC4 = graph_conv.feature_steered_convolution_layer(data=GC3,neighbors=adj_sp,sizes=sizes,translation_invariant=True,num_weight_matrices=32,num_output_channels=128)
    GC4 = tf.nn.relu(_GC4,name="GC4")
    
    
    convW3 = tf.Variable(tf.truncated_normal([1,128,4],dtype=tf.float64),name="convW3")
    convB3 = tf.Variable(tf.truncated_normal([4],dtype=tf.float64),name="convB3")
    _conv3 = tf.add(tf.nn.conv1d(GC4,convW3,1,'SAME',name="conv3"),convB3)
    conv3 = tf.nn.relu(_conv3)
    
    mask = tf.expand_dims(tf.sequence_mask(sizes,V,dtype=tf.float64),-1)
    
    Guess = tf.nn.softmax(conv3,axis=1)
    
    GC = tf.argmax(conv3[:,:,0],axis=1)
    GL = tf.argmax(conv3[:,:,1],axis=1)
    GR = tf.argmax(conv3[:,:,2],axis=1)
    
    GuessIndices = tf.transpose(tf.stack((GC,GL,GR)))
    
    B = tf.gather_nd(V0,GuessIndices)
    
    result = tf.multiply(conv3,mask)


#####################
    
    classWeights = tf.expand_dims(tf.constant([1,1,1,0],dtype=tf.float64),-1)
#    classWeights = tf.Variable(tf.truncated_normal([4],dtype=tf.float64))
    zero = tf.constant(0,dtype=tf.float64)
    one = tf.constant(1,dtype=tf.float64)
    scale = tf.add(tf.add(tf.scalar_mul(0.001,tf.cast(global_step,dtype=tf.float64)),1),-1)
    
    vertexWeights = tf.expand_dims(tf.add(one,tf.scalar_mul(scale,tf.pow(zero,L[:,:,3]))),2)
    ClassWeights =  tf.einsum('ijk,lk->ijl',vertexWeights,classWeights) 
#    ClassWeights = tf.reduce_sum(tf.multiply(classWeights,Labels),axis=2)3.
#####################    
#    _Loss = tf.nn.softmax_cross_entropy_with_logits_v2(labels=L,logits=result,axis=2)
#    Loss = tf.reduce_sum(tf.multiply(ClassWeights,_Loss))
    soft = tf.nn.softmax(result,2)
    _Loss = tf.multiply(tf.subtract(soft,L),ClassWeights)
    Loss = tf.reduce_sum(tf.square(_Loss))
    
    optimizer = tf.train.AdamOptimizer(learning_rate=.001).minimize(Loss,global_step=global_step)

print("loaded graph")  

#options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
#run_metadata = tf.RunMetadata()
#fetched_timeline = timeline.Timeline(run_metadata.step_stats)
#chrome_trace = fetched_timeline.generate_chrome_trace_format()

init_op = tf.global_variables_initializer()
training_init_op = iterator.make_initializer(trainSet,name="InitTrain")
####################################Session Definition#############################
#sess = tf.InteractiveSession(config=config)
#tf.global_variables_initializer().run()
#fd = {_V0:Vertices,_T:Triangles,_L:Labels,_sizes:sz}
#sess.run(training_init_op,feed_dict=fd)

##################################################
####################################################
sess = tf.InteractiveSession(config=config)

sess.run(tf.global_variables_initializer())
Trainables = tf.trainable_variables()
Trainables.append(optimizer)
Trainables.append(Loss)
print("First initialization")
fd = {_V0:Vertices,_T:Triangles,_L:Labels,_sizes:sz}

sess.run(training_init_op,feed_dict=fd)
print("Second initialization")
epochs = 2000
Losses = []
Before = sess.run(tf.trainable_variables())

CheckDict = {}
Checkpoints = [0,100,500,1500,2000]
#checkVars = ['U1','C1','W1','B1','U2','C2','W2','B2','U3','C3','W3','B3','U4','C4','W4','B4','CV1','CB1','_','QQ']
#_checkVars = Trainables
_checkVars = [GC1,GC2,GC3,GC4,conv3,optimizer,scale,Loss]
checkVars = ["GC1","GC2","GC3","GC4","conv4","_","Scale","QQ"]


#sess.run(optimizer, options=options, run_metadata=run_metadata)

for ci in checkVars:
    CheckDict[ci] = []

for i in range(epochs):
    if i in Checkpoints:
        RR = sess.run(_checkVars)
        for j in range(len(checkVars)):
            CheckDict[checkVars[j]].append(RR[j])
        Q = RR[-1]
        S = RR[-2]
    else:
        _,Q,S = sess.run([optimizer,Loss,scale])
    print(i,Q,round(S,2))
    Losses.append(Q)
    
    
    
Losses = np.array(Losses)
After = sess.run(tf.trainable_variables())
import matplotlib.pyplot as plt
plt.plot(Losses)
plt.show()
plt.plot(Losses[100:])
plt.show()

a,b,d = sess.run([soft,L,_Loss])


def A(i):
    global AA
    AA = (Before[i]-After[i])/(Before[i]+After[i])
    return np.average(abs(AA))    

for i in range(len(Before)):
    print(i,A(i))
    
for cv in checkVars:
    if cv != '_' and cv != 'QQ':
        thisVar = CheckDict[cv][-1]
        ThisVar = np.where(thisVar < 0,0,thisVar)
        nonZeros = len(np.nonzero(ThisVar)[0])
        fullSize = ThisVar.shape[1]*ThisVar.shape[2]
        blockedCells = fullSize-nonZeros
        percentage = (blockedCells/fullSize)*100
        print(cv,blockedCells,"/",fullSize,"("+str(int(percentage))+"%)")
        
        
#traceFile = r'C:\Users\Arren Bustamante\Documents\OBJ Snapshots\SizeReductions\ChromeTrace'
#with open(traceFile,'w') as f:
#    f.write(chrome_trace)
#import os
#os.startfime(traceFile)

